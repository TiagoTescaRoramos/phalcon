<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProdutosController
 *
 * @author tiago
 */

use \Phalcon\Mvc\Controller;

class ProdutosController extends Controller{
    public function IndexAction(){
        if(isset($_GET['idCategoria']) && $_GET['idCategoria']){
            $categoriaSelecionada = Categoria::findFirst($_GET['idCategoria']);
            $this->view->setVar('titulo',$categoriaSelecionada->getNome());

            $this->view->setVar('produtos', CategoriaProduto::find("idCategoria={$_GET['idCategoria']}"));
        }else if(isset($_POST['search'])){
            $this->view->setVar('titulo','Produtos Encontrados');

            $this->view->setVar('produtos', Produto::find("nome like '%{$_POST['search']}%'"));
        }
        $this->view->setVar('view','Produtos/ListaProdutos');
        $this->view->pick('Principal/Principal');
    }
    
    public function VisualizarProdutoAction(){
        $this->view->setVar('dadosProduto', Produto::findFirst($_POST['idProduto']));
        
        $quantidade = $this->redis->get($_POST['idProduto']);
        
        $this->view->setVar('quantidade',($quantidade) ? $quantidade : '0');
        $this->view->setVar('view','Produtos/VisualizarProduto');
        $this->view->pick('Principal/Principal');
    }
}
