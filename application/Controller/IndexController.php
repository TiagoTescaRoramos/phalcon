<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author tiago
 */

use Phalcon\Mvc\Controller;

class IndexController extends Controller{
    public function IndexAction(){
        $this->view->setVar('categorias',Categoria::find());
        $this->view->setVar('view','Principal/Home');
        $this->view->pick('Principal/Principal');
    }
}
