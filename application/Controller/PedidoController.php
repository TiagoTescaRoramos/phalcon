<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PedidoController
 *
 * @author tiago
 */

use Phalcon\Mvc\Controller;

class PedidoController extends Controller{
    public function AddCarrinhoAction(){
        if($this->redis->get($_POST['idProduto'])){
            $return['count'] = false;
        }else{
            $return['count'] = true;
        }
        
        $this->redis->set($_POST['idProduto'],$_POST['quantidade']);
        echo json_encode($return);
        $this->view->disable();
    }
    
    public function produtosCarrinho($visualizar=false){
        $prodCar = $this->redis->keys('*');
        
        if(is_array($prodCar) && count($prodCar) >= 1){
            $prod = '<table style="width:100%;">';
            $prod .= '<tr>';
            $prod .= "<th>Nome</th>";
            $prod .= "<th>Quantidade</th>";
            $prod .= "<th>Valor</th>";
            $prod .= "<th></th>";
            $prod .= '</tr>';
            $total = 0;
            foreach($prodCar as $value){
                $produto = Produto::findFirst($value);
                if(is_object($produto)){
                    $prod .= "<tr id='trPro_{$produto->getIdProduto()}'>";
                    $prod .= "<td>{$produto->getNome()}</td>";
                    $prod .= "<td>{$this->redis->get($value)}</td>";
                    $valor = $produto->getPreco() * $this->redis->get($value);
                    $total += $valor;
                    $prod .= '<td>R$ ' . number_format($valor,2,',','.') . '</td>';

                    if(!$visualizar) {
                        $prod .= "<td><input type='button' class='btn-danger' value='E' onclick='Principal.delProduto({$produto->getIdProduto()});'/></td>";
                    }
                    $prod .= '</tr>';
                }
            }
            $prod .= '<tr>';
            $prod .= "<th colspan='2'>Total</th>";
            $prod .= "<td>R$ ".number_format($total,2,',','.')."</td>";
            $prod .= '<tr>';
            $prod .= '</table>'; 
        }else{
            $prod = 'Seu carrinho esta vazio.';
        }
        return ($prod);
    }


    public function verCarrinhoAction(){
        $html = '<div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Carrinho</h4>
                    </div>
                    <div class="modal-body">
                        <p>' . $this->produtosCarrinho() . '</p>
                    </div>
                    <div class="modal-footer">
                        <form action="?_url=/Pedido/efetuarPedido" method="POST">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Fechar Compra</button>
                        </form>
                    </div>
                </div>
            </div>';
        
        echo utf8_encode($html);
        $this->view->disable();
    }
    
    public function delProdutoCarrinhoAction(){
        $this->redis->del($_POST['idProduto']);
    }
    
    public function efetuarPedidoAction(){
        $this->view->setVar('view','Pedido/efetuarPedido');
        $this->view->setVar('produtosCarrinho',$this->produtosCarrinho());
        $this->view->pick('Principal/Principal');
    }
    
    public function TerminarPedidoAction(){
        $prodsRedis = $this->redis->keys('*');
        if(count($prodsRedis) > 0 && $_POST){
            $cliente = new Cliente();
            $cliente->setNome($_POST['nome']);
            $cliente->setEmail($_POST['email']);
            $cliente->setEndereco($_POST['endereco']);
            $cliente->setCep($_POST['cep']);
            $cliente->setBairro($_POST['bairro']);
            $cliente->setCidade($_POST['cidade']);
            $cliente->setCpf($_POST['cpf']);
            $cliente->save();

            $pedido = new Pedido();
            $pedido->setIdCliente($cliente->getIdCliente());
            $pedido->setDataPedido(date('Y-m-d'));
            $pedido->setDataInclusao(date('Y-m-d H:i:s'));
            $pedido->save();

            $this->view->setVar('produtosCarrinho',$this->produtosCarrinho(true));
            foreach ($prodsRedis as $idProduto){
                $pedidoProduto = new PedidoProduto();
                $pedidoProduto->setIdPedido($pedido->getIdPedido());
                $pedidoProduto->setIdProduto($idProduto);
                $pedidoProduto->setQuantidade($this->redis->get($idProduto));
                $pedidoProduto->save();
                $this->redis->del($idProduto);
            }

            $this->view->setVar('pedido',$pedido);
            $this->view->setVar('cliente',$cliente);

        }
        $this->view->setVar('view','Pedido/resumoPedido');
        $this->view->pick('Principal/Principal');
    }
}
