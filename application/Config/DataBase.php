<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataBase
 *
 * @author tiago
 */
class DataBase {
    private $host;
    private $username;
    private $password;
    private $base;
    
    public function __construct() {
        $this->host = 'localhost';
        $this->username = 'root';
        $this->password = 'karenzinha';
        $this->base = 'sistemaCompras';
    }

    public function getHost() {
        return $this->host;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getBase() {
        return $this->base;
    }
}
