<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Path
 *
 * @author tiago
 */
class Path {
    private $view;
    private $controller;
    private $library;
    private $model;
    
    public function __construct() {
        $this->view = APP_PATH.'application/View';
        $this->controller = APP_PATH.'application/Controller';
        $this->library = APP_PATH.'application/Library';
        $this->model = APP_PATH.'application/Model';
    }
    
    public function getView() {
        return $this->view;
    }

    public function getController() {
        return $this->controller;
    }

    public function getLibrary() {
        return $this->library;
    }

    public function getModel() {
        return $this->model;
    }
}
