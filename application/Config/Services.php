<?php
include_once(APP_PATH.'application/Config/Path.php');
include_once(APP_PATH.'application/Config/DataBase.php');

class Services{
    private $DI;
    private $Path;
    private $DataBase;
    private $Application;

    public function __construct() {
        $this->Path = new Path();
        $this->DataBase = new DataBase();
        $this->DI = new Phalcon\DI\FactoryDefault();
    }
    
    private function Registers(){
        $LOAD = new \Phalcon\Loader();
        $LOAD->registerDirs(
            Array(
                $this->Path->getController(),
                $this->Path->getModel(),
                $this->Path->getLibrary()
            )
        )->register();
    }
    
    private function setView(){
        $this->DI->set('view',function(){
            $view = new \Phalcon\Mvc\View();
            $view->setViewsDir($this->Path->getView());
            return $view;
        });
    }
    
    private function setDb(){
        $this->DI->set('db',function(){
            return new \Phalcon\Db\Adapter\Pdo\Mysql(Array(
                'host' => $this->DataBase->getHost(),
                'username' => $this->DataBase->getUsername(),
                'password' => $this->DataBase->getPassword(),
                'dbname' => $this->DataBase->getBase()
            ));
        });
    }
    
    private function setRedis(){
        $this->DI->set('redis',function(){
            include_once(APP_PATH.'vendor/autoload.php');
            
            return new Predis\Client(Array(
                'host' => 'localhost',
                'port' => 6379,
'database' => '50'
            ));
        });       
    }


    public function getContent(){
        $this->Registers();
        $this->setView();
        $this->setDb();
        $this->setRedis();
        $Application = new \Phalcon\Mvc\Application($this->DI);
        return $Application->handle()->getContent();
    }
}
