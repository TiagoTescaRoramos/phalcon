/**
 * Created by tiago on 21/05/15.
 */
Principal = function(){
    this.AddCarrinho = function(idProduto){
        if(jQuery('#quantidade').val() > 0){
            jQuery.ajax({
                type: 'POST',
                async: false,
                dataType:'JSON',
                url: '?_url=/Pedido/AddCarrinho',
                data:{
                    'idProduto': idProduto,
                    'quantidade': jQuery('#quantidade').val()
                },
                success: function(res){
                    if(res.count){
                        jQuery('#qtd').html(parseInt(jQuery('#qtd').html())+1);
                    }
                    alert('Produto Adicionado ao Carrinho.')
                }
            });
        }else{
            alert('Informa a Quantidade desejada.');
        }
    }
    
    this.verCarrinho = function(){
        jQuery.ajax({
            type: 'POST',
            async: false,
            dataType:'html',
            url: '?_url=/Pedido/verCarrinho',
            data:{
                
            },
            success: function(res){
                jQuery('#alert').html(res);
                jQuery('#alert').modal('show');
            }
        });
    }
    
    this.delProduto = function(idProduto){
        jQuery.ajax({
            type: 'POST',
            async: false,
            dataType:'html',
            url: '?_url=/Pedido/delProdutoCarrinho',
            data:{
                idProduto: idProduto
            },
            success: function(res){
                jQuery('#trPro_'+idProduto).hide(700,function(){
                    jQuery('#qtd').html(parseInt(jQuery('#qtd').html())-1);
                    jQuery(this).remove();
                });
            }
        });
    }    
}

var Principal = new Principal();

jQuery(document).ready(function(){
    jQuery('#carrinho').click(function(){
        Principal.verCarrinho();
    });

    jQuery('.input_cep').each(function () {
        jQuery(this).mask('99999-999');
    });

    jQuery('.input_numeroInt').each(function () {
        jQuery(this).mask('99999999999');
    });

    jQuery('.input_cpf').each(function () {
        jQuery(this).mask('999.999.999-99');
    });
});