<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PedidoProduto
 *
 * @author tiago
 */
class PedidoProduto extends Phalcon\Mvc\Model{
    protected $idPedido;
    protected $idProduto;
    protected $quantidade;


    public function getSource(){
        return 'pedidoProduto';
    }
    
    public function initialize(){
        $this->belongsTo('idPedido','Pedido','idPedido');
        $this->belongsTo('idProduto','Produto','idProduto');
    }

    public function getIdPedido() {
        return $this->idPedido;
    }

    public function getIdProduto() {
        return $this->idProduto;
    }
    
    public function getQuantidade() {
        return $this->quantidade;
    }

    public function setIdPedido($idPedido) {
        $this->idPedido = $idPedido;
    }

    public function setIdProduto($idProduto) {
        $this->idProduto = $idProduto;
    }
    
    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }
}
