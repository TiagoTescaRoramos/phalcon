<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Produto
 *
 * @author tiago
 */
class Produto extends Phalcon\Mvc\Model{
    protected $idProduto;
    protected $nome;
    protected $descricao;
    protected $foto;
    protected $preco;
    protected $idCaracteristicaProduto;
    protected $ativo;

    public function getSource(){
        return 'produto';
    }
    
    public function initialize(){
        $this->belongsTo('idCaracteristicaProduto','CaracteristicaProduto','idCaracteristicaProduto');
    }


    public function setIdProduto($idProduto) {
        $this->idProduto = $idProduto;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setFoto($foto) {
        $this->foto = $foto;
    }

    public function setPreco($preco) {
        $this->preco = $preco;
    }

    public function setIdCaracteristicaProduto($idCaracteristicaProduto) {
        $this->idCaracteristicaProduto = $idCaracteristicaProduto;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }
    
    public function getIdProduto() {
        return $this->idProduto;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getFoto() {
        return SERVER . "application/Resources/img/produtos/{$this->foto}";
    }

    public function getPreco() {
        return $this->preco;
    }

    public function getIdCaracteristicaProduto() {
        return $this->idCaracteristicaProduto;
    }

    public function getAtivo() {
        return $this->ativo;
    }
}
