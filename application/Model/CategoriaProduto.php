<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoriaProduto
 *
 * @author tiago
 */
class CategoriaProduto extends Phalcon\Mvc\Model{
    protected $idCategoria;
    protected $idProduto;
    
    public function getSource(){
        return 'categoriaProduto';
    }

    public function initialize(){
        $this->belongsTo('idCategoria','Categoria','idCategoria');
        $this->belongsTo('idProduto','Produto','idProduto');
    }
    
    public function getIdCategoria() {
        return $this->idCategoria;
    }

    public function getIdProduto() {
        return $this->idProduto;
    }

    public function setIdCategoria($idCategoria) {
        $this->idCategoria = $idCategoria;
    }

    public function setIdProduto($idProduto) {
        $this->idProduto = $idProduto;
    }
}
