<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cliente
 *
 * @author tiago
 */
class Cliente extends Phalcon\Mvc\Model{
    protected $idCliente;
    protected $nome;
    protected $email;
    protected $endereco;
    protected $cep;
    protected $bairro;
    protected $cidade;
    protected $cpf;
    protected $ativo;
    
    public function getSource(){
        return 'cliente';
    }
    
    public function getIdCliente() {
        return $this->idCliente;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getEndereco() {
        return $this->endereco;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getBairro() {
        return $this->bairro;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function getCpf() {
        return $this->cpf;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function setIdCliente($idCliente) {
        $this->idCliente = $idCliente;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    public function setCep($cep) {
        $this->cep = $cep;
    }

    public function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }
}
