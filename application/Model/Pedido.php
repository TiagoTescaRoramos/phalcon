<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pedido
 *
 * @author tiago
 */
class Pedido extends Phalcon\Mvc\Model{
    protected $idPedido;
    protected $idCliente;
    protected $dataPedido;
    protected $dataInclusao;
    
    public function getSource(){
        return 'pedido';
    }

    public function initialize(){
        $this->belongsTo('idCliente','Cliente','idCliente');
    }
    
    public function getIdPedido() {
        return $this->idPedido;
    }

    public function getIdCliente() {
        return $this->idCliente;
    }

    public function getDataPedido() {
        return $this->dataPedido;
    }

    public function getDataInclusao() {
        return $this->dataInclusao;
    }

    public function setIdPedido($idPedido) {
        $this->idPedido = $idPedido;
    }

    public function setIdCliente($idCliente) {
        $this->idCliente = $idCliente;
    }

    public function setDataPedido($dataPedido) {
        $this->dataPedido = $dataPedido;
    }

    public function setDataInclusao($dataInclusao) {
        $this->dataInclusao = $dataInclusao;
    }
}
