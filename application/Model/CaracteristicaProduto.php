<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CaracteristicaProduto
 *
 * @author tiago
 */
class CaracteristicaProduto extends Phalcon\Mvc\Model{
    protected $idCaracteristicaProduto;
    protected $caracteristica;
    protected $ativo;
    
    public function getSource(){
        return 'caracteristicaProduto';
    }
    
    public function setIdCaracteristicaProduto($idCaracteristicaProduto) {
        $this->idCaracteristicaProduto = $idCaracteristicaProduto;
    }

    public function setCaracteristica($caracteristica) {
        $this->caracteristica = $caracteristica;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }
    
    public function getIdCaracteristicaProduto() {
        return $this->idCaracteristicaProduto;
    }

    public function getCaracteristica() {
        return $this->caracteristica;
    }

    public function getAtivo() {
        return $this->ativo;
    }
}
