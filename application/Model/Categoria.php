<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 12/06/15
 * Time: 18:10
 */

class Categoria extends Phalcon\Mvc\Model{
    protected $idCategoria;
    protected $nome;
    protected $ativo;
    
    /**
     * @param mixed $idCategoria
     */
    public function setIdCategoria($idCategoria)
    {
        $this->idCategoria = $idCategoria;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }
    
    public function getIdCategoria() {
        return $this->idCategoria;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getAtivo() {
        return $this->ativo;
    }
}