<?php
use Phalcon\Mvc\Application;
use Phalcon\Config\Adapter\Ini as ConfigIni;

try{
    define('APP_PATH', realpath('') . '/');
    define('SERVER',"http://{$_SERVER['SERVER_NAME']}:{$_SERVER['SERVER_PORT']}/");
    
    include_once(APP_PATH.'application/Config/Services.php');
    
    $service = new Services();
    echo $service->getContent();
    
} catch (\Phalcon\Exception $ex) {
    echo "PhalconException: {$ex->getMessage()} {$ex->getFile()}";
}
