CREATE DATABASE sistemaCompras;

USE sistemaCompras;

CREATE TABLE categoria(
	idCategoria int primary key auto_increment,
	nome varchar(255),
	ativo tinyint(1) default 1
)ENGINE=InnoDB;

INSERT INTO `sistemaCompras`.`categoria` (`nome`) VALUES ('P�es');
INSERT INTO `sistemaCompras`.`categoria` (`nome`) VALUES ('Bolo ');
INSERT INTO `sistemaCompras`.`categoria` (`nome`) VALUES ('Salgadinhos');
INSERT INTO `sistemaCompras`.`categoria` (`nome`) VALUES ('Refrigerante');
INSERT INTO `sistemaCompras`.`categoria` (`nome`) VALUES ('Tortas');
INSERT INTO `sistemaCompras`.`categoria` (`nome`) VALUES ('Docinhos');


CREATE TABLE caracteristicaProduto(
	idCaracteristicaProduto int primary key auto_increment,
	caracteristica text,
	ativo tinyint(1) default 1
)ENGINE=InnoDB;

insert into caracteristicaProduto(idCaracteristicaProduto,caracteristica) value(1,'M�dio 50gr. Ingredientes: Farinha de Trigo, a�ucar, gordura Vegetal, Sal e Fermento');
insert into caracteristicaProduto(idCaracteristicaProduto,caracteristica) value(2,'Bolo de 2 Kilos. Massa: Farinha de Trigos, ovos, fermento, chocolate em p� e a�ucar. Recheio: Leite condensado, chocolate em p� e margarina. Cobertura: Leite condensado com chocolate em p� com raspas de chocolate.');
insert into caracteristicaProduto(idCaracteristicaProduto,caracteristica) value(3,'Ingredientes: Farinha, ovos, leite, frango, salsinha e sal.');


CREATE TABLE produto(
	idProduto int primary key auto_increment,
	nome varchar(255),
	descricao text,
	foto varchar(255),
	preco decimal(10,2),
	idCaracteristicaProduto int,
	ativo tinyint(1) default 1,
	constraint produto_idCaracteristicaProduto_FK foreign key(idCaracteristicaProduto) references caracteristicaProduto(idCaracteristicaProduto)
)ENGINE=InnoDB;

insert into produto
(idProduto,idCaracteristicaProduto,nome,descricao,foto,preco,ativo)
values(1,1,'P�o com Carne','P�o franc�s com Carne Louca','pao_carne2.jpg',1.89,1);

insert into produto
(idProduto,idCaracteristicaProduto,nome,descricao,foto,preco,ativo)
values(2,2,'Bolo de Chocolate','Bolo com chocolate ralado','bolo_chocolate_1.jpg',40.99,1);

insert into produto
(idProduto,idCaracteristicaProduto,nome,descricao,foto,preco,ativo)
values(3,3,'Coxinha de Frango','Coxinha com frango desfiado','coxinha-3.jpg',0.99,1);

CREATE TABLE categoriaProduto(
	idProduto int,
	idCategoria int,
	constraint categoriaProduto_idProduto_FK foreign key(idProduto) references produto(idProduto),
	constraint categoriaProduto_idCategoria_FK foreign key(idCategoria) references categoria(idCategoria),
	constraint categoriaProduto_PK primary key(idProduto,idCategoria)
)ENGINE=InnoDB;

CREATE TABLE cliente(
	idCliente int primary key auto_increment,
	nome varchar(255),
	email varchar(255),
	endereco varchar(255),
	cep varchar(8),
	bairro varchar(150),
	cidade varchar(150),
	cpf varchar(11),
	ativo tinyint(1) default 1
)ENGINE=InnoDB;

CREATE TABLE pedido(
	idPedido int primary key auto_increment,
	idCliente int,
	dataPedido date,
	dataInclusao datetime,
	constraint pedido_idCliente_FK foreign key(idCliente) references cliente(idCliente)
)ENGINE=InnoDB;

CREATE TABLE pedidoProduto(
	idPedido int,
	idProduto int,
        quantidade int,
        constraint pedidoProduto_PK primary key(idPedido,idProduto)
)ENGINE=InnoDB;

insert into caracteristicaProduto(idCaracteristicaProduto,caracteristica) 
value(4,'Brigadeiro, Beijinhos e Bicho de P�.');

insert into caracteristicaProduto(idCaracteristicaProduto,caracteristica) 
value(5,'Ingrediente: Leite Condensado, chocolate derretido e bolacha de maizena.');

insert into caracteristicaProduto(idCaracteristicaProduto,caracteristica) 
value(6,'3 Garrafas de 2 litros');

insert into produto
(idProduto,idCaracteristicaProduto,nome,descricao,foto,preco,ativo)
values(6,6,'Refrigerante','1 Kit de Fanta, Coca Cola e Guarana','refrigerantes4.jpg',14.99,1);

insert into produto
(idProduto,idCaracteristicaProduto,nome,descricao,foto,preco,ativo)
values(5,5,'Torta Holandesa','Torta Holandesa com Chocolate','tortaholandesa_6.jpg',29.90,1);

insert into produto
(idProduto,idCaracteristicaProduto,nome,descricao,foto,preco,ativo)
values(4,4,'Docinhos','100 Docinhos Sortidos','docinhos_5.jpg',59.90,1);

INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (1,1);
INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (2,2);
INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (3,3);
INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (4,6);
INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (5,6);
INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (5,5);
INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (5,2);
INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (2,6);
INSERT INTO `categoriaProduto` (`idProduto`,`idCategoria`) VALUES (6,4);
